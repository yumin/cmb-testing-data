ExportSimDakota

To recreate test:
File-Open simplebox.cmb from testing-data/model/3d/cmb
File-Open dakota_example.sbt from testing-data/simulation_workflow/Dakota
Switch to Attribute tab
Set "Method List" to "test" (enter as text)
Select "Method" tab
Select the "Nondeterministic Sampling Method"
Click New.
Set "Method Name" to "test"
Check "Random Number Generator"
Select "Variables" Tab
Click New. Set Variable Name to "aa"
File-Export Simulation File
Select "Model A" as "Model"
Select "dakotaExporter.py" from testing-data as "Python Script"
Save to test.bc in the testing-data folder.
 * Note: Manually edit the script afterward to point to testing/Temporary
Export and Stop Recording
